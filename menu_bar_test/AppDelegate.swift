//
//  AppDelegate.swift
//  menu_bar_test
//
//  Created by SubaruShiozaki on 2018/08/07.
//  Copyright © 2018年 SubaruShiozaki. All rights reserved.
//

import Cocoa
import Foundation

@NSApplicationMain
@objcMembers
class AppDelegate: NSObject, NSApplicationDelegate {

    //メニューとアウトレット接続
    @IBOutlet weak var menu: NSMenu!
    
    //メニューバーに表示されるアプリを作成
    let statusItem = NSStatusBar.system.statusItem(withLength: NSStatusItem.variableLength)
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
        
        // メニューバーに表示されるアプリ。今回は文字列で設定
        self.statusItem.title = "notiy"
        // メニューのハイライトモードの設定
        self.statusItem.highlightMode = true
        // メニューの指定
        self.statusItem.menu = menu

        // Quit機能の作成
        let quitBotton = NSMenuItem()
        quitBotton.title = "Quit"
        quitBotton.action = #selector(AppDelegate.quit(_:))
        
        //APITEST
        let urlString = "https://www.gitlab.com/api/v4/projects/7712561/repository/branches"
        let queryItems = [URLQueryItem(name: "private_token", value: "uYrdTf3BWsLk39PZpuM6")]
        api_test(url: urlString,queryItems: queryItems)
        

        
        
        self.statusItem.menu?.addItem(quitBotton)
        
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
    
    func quit(_ sender:Any) {
        NSApplication.shared.terminate(self)
    }
    
    func showLogs(_ sender:Any) {
        menu.item(at: 2)
    }
    
    @IBAction func accept(_ sender:Any){
        print("push the accept")
    }
    
    func api_test(url urlString: String, queryItems: [URLQueryItem]? = nil){
        
        var compnents = URLComponents(string: urlString)
        compnents?.queryItems = queryItems
        let url = compnents?.url
        
        
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            
            if let data = data, let response = response {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments)
                    print(json)
                } catch {
                    print("Serialize Error")
                }
            } else {
                print(error ?? "Error")
            }
        }
        task.resume()
        
        }
}

